// Update with your config settings.
var conf = require("./config.json");

module.exports = {
  development: {
    client: "oracledb",
    connection: {
      host: conf.dbConfig.db.host,
      database: conf.dbConfig.db.database,
      user: conf.dbConfig.db.user,
      password: conf.dbConfig.db.password,
      ssl: false
    }
  },
};

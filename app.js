const express = require('express');
const bodyParser = require('body-parser');
const cors = require('cors');
const path = require('path');

const pool = require('./database/connection');
const router = require('./routes/bulk');
const app = express();
const PORT = process.env.port || 2000;

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }))

// //-==================== PATH SETTING =================
app.use(express.static(path.join(__dirname, "public")));
// //======================================================//

var corsOptions = {
    methods: ['GET','POST','DELETE','UPDATE','PUT','PATCH'],
    origin: true, // sesuai sama
    credentials:true,   
    optionsSuccessStatus: 200 // some legacy browsers (IE11, various SmartTVs) choke on 204
}

app.use(cors(corsOptions));
app.use(router);

app.get('*', (req, res) => { 
    res.sendFile(path.join(__dirname, "/public/index.html"));
 });

app.listen(PORT, () => {
    console.log(`{server jalan di port ${PORT}}`);
})
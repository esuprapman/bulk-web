const pool = require("../database/connection");

const addData = async (req, res) => {
    try {
        const {key, value, desc} = req.body;    
        // console.log(key, value, desc);
        let lastRecordID = await pool
            .select('ID')
            .from('GENERAL_SETTING')
            .orderBy('ID', 'desc').first();
        
        // console.log(lastRecordID.ID);

        const save = await pool('GENERAL_SETTING')
            .insert({
                ID: ++lastRecordID.ID,
                DESCRIPTION: desc,
                KEY:key,
                VALUE: value
            });
        
        if (!save) {
            res.status(203).json(`Failed add data`);
        }        
        // console.log(save);
        res.status(200).json({status: 200,msg:`Successfully added data`});
        
    } catch (error) {
        res.status(500).json(`Server : ${error}`)
    } 
}

const updateData = async (req, res) => {
    try {
        const {id, key, value, desc} = req.body;    
        console.log(id,key, value, desc);

        const update = await pool('GENERAL_SETTING')
            .update({
                DESCRIPTION: desc,
                KEY : key,
                VALUE : value
            })
            .where({
                "ID" : id
            });
        // console.log(update);
        res.status(200).json(`Successfully updated data`);
    } catch (error) {
        res.status(500).json(`Server : ${error}`)
    }   
}

const deleteData = async (req, res) => {
    try {
        const {id} = req.params;    
        console.log(id);
        const deleteData = await pool('GENERAL_SETTING').delete().where({"ID" : id});
        
        res.status(200).json(`Successfully deleted data`);
    } catch (error) {
        res.status(500).json(`Server : ${error}`)
    }   
}

const getData = async (req, res) => {
    try {
        const data = await pool
            .select('*')
            .from('GENERAL_SETTING')
            .orderBy('ID', 'ASC');

        res.status(200).json(data);
    } catch (error) {
        res.status(500).json(`Server : ${error}`)
    }   
}

module.exports = {addData, getData, updateData, deleteData};
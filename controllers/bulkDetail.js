const pool = require("../database/connection")
const db = require('../database/connection');

const json2csv = require('json2csv').parse

const searchBulkDetail = async (req, res) => {
    try {
        const params = req.params.id;
        const { search_master, filter, status } = req.body;
        console.log(params);
        // console.log(search_master, `bd.${filter}`, status);
        if (filter === "") {
            return res.status(400).json({ msg: 'Filter tidak boleh kosong!' });

        }

        const validFilters = ["REQUEST_ID", "RECEIVING_BIC", "ERROR_MESSAGE"];

        if (!validFilters.includes(filter)) {
            return res.status(400).json({ msg: 'Data Filter salah!' });
        }

        const validStatuses = ["INIT", "SUCCESS", "RECEIVED", "COMPLITED", "FAILED", "REJECT", "INPROGRESS"];

        let query = pool.select("bd.*", "bm.BULK_ID")
            .from("BULK_DETAIL as bd")
            .leftJoin("BULK_MASTER as bm", "bm.ID", "bd.MASTER_ID")
            .where({
                "bd.MASTER_ID": params,
                [`bd.${filter}`]: search_master
            })
            .orderBy("bd.RECORD_NO", "asc");

        if (status !== "") {
            if (!validStatuses.includes(status)) {
                return res.status(400).json({ msg: 'Data status salah!' });
            }
            query = query.andWhere({ "bd.STATUS": status });
        }

        const data = await query;
        res.status(200).json(data);

        // if (filter !== "") {
        //     console.log('filter masuk');
        //     if (filter === "REQUEST_ID" || filter === "RECEIVING_BIC" || filter === "ERROR_MESSAGE") {
        //         if (status !== "") {
        //             console.log('status masuk');
        //             if (status === "INIT" || status === "SUCCESS" || status === "RECEIVED" || status === "COMPLITED" || status === "FAILED" || status === "REJECT", status === "INPROGRESS") {
        //                 const data = await pool
        //                     .select("bd.*", "bm.BULK_ID")
        //                     .from("BULK_DETAIL as bd")
        //                     .leftJoin("BULK_MASTER as bm", "bm.ID", "bd.MASTER_ID")
        //                     .where({
        //                         "bd.MASTER_ID": params,
        //                         [`bd.${filter}`]: search_master,
        //                         "bd.STATUS": status
        //                     }).orderBy("bd.RECORD_NO", "desc", "first");

        //                 res.status(200).json(data)
        //             } else {
        //                 res.status(400).json({ msg: 'Data status salah!' });
        //             }
        //         } else {
        //             const data = await pool
        //                 .select("bd.*", "bm.BULK_ID")
        //                 .from("BULK_DETAIL as bd")
        //                 .leftJoin("BULK_MASTER as bm", "bm.ID", "bd.MASTER_ID")
        //                 .where({
        //                     "bd.MASTER_ID": params,
        //                     [`bd.${filter}`]: search_master
        //                 })
        //                 .orderBy("bd.RECORD_NO", "desc", "first");
        //             res.status(200).json(data)
        //         }
        //     } else {
        //         res.status(400).json({ msg: 'Data Filter salah!' });
        //     }
        // } else {
        //     res.status(400).json({ msg: 'Filter tidak boleh kosong!' });
        // }

    } catch (error) {
        res.status(500).json();
        console.log('Database : ' + error);
    }

}

const getDataDetail = async (req, res) => {
    try {
        const params = req.params.id;
        // console.log(params);
        const data = await pool.select("bd.*", "bm.BULK_ID")
        .from("BULK_DETAIL as bd")
        .leftJoin("BULK_MASTER as bm", "bm.ID", "bd.MASTER_ID")
        .where("bd.MASTER_ID", params);
        
        res.status(200).json(data)

    } catch (error) {
        res.status(500).json();
        console.log('Database : ' + error);
    }
}

module.exports = { getDataDetail, searchBulkDetail};
const pool = require("../database/connection")
const db = require('../database/connection');



const getDataHistory = async (req, res) => {
    try {
        const params = req.params.id;
        // const data = await pool.select("*").from("BULK_RESPONSE_HISTORY").where("MASTER_ID", params);

        const data = await pool
            .select("brh.*", "bm.BULK_ID")
            .from("BULK_RESPONSE_HISTORY as brh")
            .leftJoin("BULK_MASTER as bm", "bm.ID", "brh.MASTER_ID")
            .where("brh.MASTER_ID", params);

        res.status(200).json(data)

    } catch (error) {
        res.status(500).json();
        console.log('Database : ' + error);
    }
}

const searchBulkHistory = async (params) => {

}

module.exports = getDataHistory;
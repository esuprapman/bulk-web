// const { from } = require("json2csv/json2csvtransform");
const pool = require("../database/connection");
const db = require("../database/connection");


let select = ["ID", "BULK_ID", "SENDING_BIC",
             "REQUEST_DATE", "TOTAL_RECORD", 
             "TOTAL_ACCEPTED", "TOTAL_REJECT", 
             "TOTAL_FAIL", "STATUS", "ERROR_MESSAGE", 
             "TRANS_CODE"];

             
const searchMaster = async (req, res) => {
    // {"search_master":"wkwwk","filter":"","fromDate":"","toDate":"","transaction":"","status":""}
    try {
        const { search_master, filter, fromDate, toDate, transaction, status } = req.body;
        let databulk;

        console.log(req.body);

        async function query(whereObj) {
            return await pool.select('*').from("BULK_MASTER").where(whereObj);
            // console.log(whereObj);
        }

        //========================= VALIDATE SETIAP FORM
        // if (!filter) {
        //     return res.status(400).json({ msg: "Filter tidak boleh kosong!" });
        // }

        if (filter && !["BULK_ID", "ID", "SENDING_BIC"].includes(filter)) {
            return res.status(400).json({ msg: "Data Filter Salah!" });
        }

        if (status && !["FAILED", "COMPLETED", "RECEIVED", "PENDING", "SUCCESS"].includes(status)) {
            return res.status(400).json({ msg: "Data status Salah!" });
        }
        
        if (transaction && !["411", "400", "405"].includes(transaction)) {
            return res.status(400).json({ msg: "Data Transaction Salah!" });
        }

        let req_date = new Date(fromDate);
        let to_date = new Date(toDate);
        // console.log(typeof req_date );
        // console.log(typeof to_date );

        // ================================ CEK DATE JIKA TIDAK TERISI
        if (fromDate && toDate) {
            console.log('date passed!');

            if (status && transaction && filter && fromDate && toDate) { // CEK semua form terisi
                    console.log('all filter is filled');
        
                    const queryInDate = await pool.select('*').from("BULK_MASTER")
                    .where({
                        [filter]: search_master,
                        "STATUS": status,
                        "TRANS_CODE": transaction
                    })            
                    .whereBetween('REQUEST_DATE', [
                                                    new Date(fromDate),
                                                    new Date(toDate)
                    ]);
        
                    databulk = queryInDate;
                    res.status(200).json(databulk);
                }else if (status && filter && fromDate && toDate) { // form filter, status, dan date terisi
                    console.log('status dan filter');
                    const queryInDate = await pool.select('*').from("BULK_MASTER")
                    .where({
                        [filter]: search_master,
                        "STATUS": status,
                    })            
                    .whereBetween('REQUEST_DATE', [
                                                    new Date(fromDate),
                                                    new Date(toDate)
                    ]);
                    
                    databulk = await query(aa); //dari line 21
                    res.status(200).json(databulk);
                }
            
            // search only date
            const queryInDate = await pool.select(select).from("BULK_MASTER")          
            .whereBetween('REQUEST_DATE', [
                                            new Date(fromDate),
                                            new Date(toDate)
            ]);
            
            // console.log(queryInDate);
            databulk = queryInDate;
            res.status(200).json(databulk);
        }
        //=================== JIKA KEDUA DATE KOSONG
        else if(!fromDate && !toDate){
            if (status && transaction && filter) { // forom filter, trans, status terisi
                console.log('status, filter dan trans jalan');
                let aa = {
                    [filter]: search_master,
                    "STATUS": status,
                    "TRANS_CODE": transaction
                }
                databulk = await query(aa);
                res.status(200).json(databulk);
    
            } else if (status && filter) { // form filter, dan status terisi
                console.log('status dan filter');
                let aa = {
                    [filter]: search_master,
                    "STATUS": status
                }
                // console.log(aa);
                databulk = await query(aa);
                res.status(200).json(databulk);
    
            } else if (transaction && filter) { // form filter, dan trans terisi
                console.log('status dan filter jalan');
                let aa = {
                    [filter]: search_master,
                    "TRANS_CODE": transaction
                }
                // console.log(aa);
                databulk = await query(aa);
                res.status(200).json(databulk);
            } else if (filter) {
                let aa = {
                    [filter]: search_master
                }
                // console.log(aa);
                databulk = await query(aa);
                res.status(200).json(databulk);
            } else if (status) {
                let aa = {
                    "STATUS": status
                }
                // console.log(aa);
                databulk = await query(aa);
                res.status(200).json(databulk);
            } else if (transaction) {
                console.log('transk jalan');
    
                let aa = {
                    "TRANS_CODE": transaction
                }
                // console.log(aa);
                databulk = await query(aa);
                res.status(200).json(databulk);
            }
        }
        else if(!fromDate || ! toDate){
            return res.status(203).json('Pastikan from date dan to date harus di isi');
        }


        // if (fromDate === "" && toDate === "") {
        //     return res.status(400).json({ msg: "tanggal kosong!" });

        // }
        // const cek = data.find((value) => {
        //     return value.SENDING_BIC = "INDOIDJA";
        // })


        // if (filter !== "") {
        //     console.log('filter jalan');
        //     if (filter === "BULK_ID" || filter === "ID" || filter === "SENDING_BIC") {
        //         if (status !== "") {
        //             console.log('status jalan');
        //             if (status === "PENDING" || status === "SUCCESS" || status === "RECEIVED" || status === "COMPLITED" || status === "FAILED") {
        //                 if (transaction !== "") {
        //                     //============= JIKA SEMUA FORM TERISI ====================
        //                     console.log('transaksi jalan');
        //                     if (transaction === "411") {
        //                         const data = await pool.select(select).from("BULK_MASTER")
        //                             .where({
        //                                 [filter]: search_master,
        //                                 "STATUS": status,
        //                                 "TRANS_CODE": transaction
        //                             });
        //                         res.status(200).json(data);

        //                     }
        //                     res.status(400).json('data transaction salah!');
        //                 }

        //                 //============= JIKA TRANSACTION KOSONG ====================

        //                 const data = await pool.select(select).from("BULK_MASTER")
        //                     .where({
        //                         [filter]: search_master
        //                     });
        //                 res.status(200).json(data);

        //                 console.log('transaksi ga jalan');
        //             } else {
        //                 res.status(400).json('data status salah!');
        //             }
        //         } else {
        //             //============= JIKA STATUS KOSONG ====================

        //             if (transaction !== "") {
        //                 console.log('transaksi jalan');
        //                 if (transaction === "411") {
        //                     const data = await pool.select(select).from("BULK_MASTER")
        //                         .where({
        //                             [filter]: search_master,
        //                             "TRANS_CODE": transaction
        //                         });
        //                     res.status(200).json(data);
        //                 }
        //                 res.status(400).json('data transaction salah!');
        //             }

        //             const data = await pool.select(select).from("BULK_MASTER")
        //                 .where({
        //                     [filter]: search_master
        //                 });
        //             res.status(200).json(data);
        //         }
        //     } else {
        //         res.status(400).json({ msg: "Data filter salah!" })

        //     }
        // } else {
        //     res.status(400).json({ msg: "Filter tidak boleh kosong!" })
        // }

    } catch (error) {
        res.status(500).json();
        console.log('Server : ' + error);
    }
}

const getDataMaster = async (req, res) => {
    try {

        const data = await pool.select(select).from("BULK_MASTER")
        // dataBulk = data;
        res.setHeader('Content-Type', 'application/json');
        res.status(200).json(data);
    } catch (error) {
        res.status(500).json();
        console.log('Database : ' + error);
    }
}

module.exports = { getDataMaster, searchMaster };
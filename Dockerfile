FROM node:18
ENV NODE_ENV=development 

# Create app directory
WORKDIR /opt/app

# install oracle instant client
ADD instantclient-basiclite-linuxx64.zip /opt/app
RUN unzip instantclient-basiclite-linuxx64.zip
RUN apt update
RUN apt install libaio1 
RUN sh -c "echo /opt/app/instantclient_21_10 > /etc/ld.so.conf.d/oracle-instantclient.conf"
RUN ldconfig

# Install app dependencies
COPY ["package.json", "package-lock.json*", "./"]
RUN npm install

# Bundle app source
COPY . .

EXPOSE 2000
CMD [ "npm", "start" ]
const excelService = require('./excel-service');
const excelJS = require("exceljs");

const axios = require('axios');



const getexcel = async (req, res) => {
    const response = await axios.get('http://localhost:2000/bulk/master'); // fetching data
    const databulk = response.data; //response data
    // console.log(databulk);
    // res.status(200).json(databulk);
    const workbook = new excelJS.Workbook();  // Create a new workbook

    const worksheet = workbook.addWorksheet("My Users"); // New Worksheet

    worksheet.columns = [
        { header: "ID.", key: "id", width: 10 }, 
        { header: "BULK ID", key: "b_id", width: 15 },
        { header: "SENDING BIC", key: "bic", width: 20 },
        { header: "REQUEST DATE", key: "req_date", width: 30 },
        { header: "TOTAL RECORD", key: "record", width: 15 },
        { header: "TOTAL ACCEPTED", key: "accepted", width: 15 },
        { header: "TOTAL REJECT", key: "reject", width: 15 },
        { header: "TOTAL FAIL", key: "fail", width: 15 },
        { header: "STATUS", key: "status", width: 20 },
        { header: "ERROR MESSAGE", key: "error", width: 30 },
        { header: "TRANS CODE", key: "trans", width: 15 },
      ];
    
      await databulk.map((value, idx) => {
        worksheet.addRow({
          id : value.ID,
          b_id : value.BULK_ID,
          bic : value.SENDING_BIC,
          req_date : value.REQUEST_DATE,
          record: value.TOTAL_RECORD,
          accepted : value.TOTAL_ACCEPTED,
          reject : value.TOTAL_REJECT,
          fail : value.TOTAL_FAIL,
          status : value.STATUS,
          error: value.ERROE_MESSAGE,
          trans : value.TRANS_CODE
        })
      });

    res.writeHead(200, {
        'Content-Type': 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet',
        'Content-Disposition': 'attachment;filename=Bulk.xlsx',
    })

    workbook.xlsx.write(res);

}

module.exports = { getexcel };

const excelJS = require("exceljs");

async function buildExcel(data, dataCallback, endCallback) { 

  const workbook = new excelJS.Workbook();  // Create a new workbook

  // workbook.on('data', dataCallback);
  // workbook.on('end', endCallback);
  console.log(data);
  let bb = [
    {
        ID: 266272,
        BULK_ID: '202304170',
        SENDING_BIC: 'INDOIDJA',
        REQUEST_DATE: '2023-04-17T01:11:41.606Z',
        TOTAL_RECORD: 4,
        TOTAL_ACCEPTED: 4,
        TOTAL_REJECT: 0,
        TOTAL_FAIL: 0,
        STATUS: 'COMPLETED',
        ERROR_MESSAGE: null,
        TRANS_CODE: '411'
    },
    {
        ID: 266285,
        BULK_ID: '456789011',
        SENDING_BIC: 'INDOIDJA',
        REQUEST_DATE: '2023-04-17T09:31:43.950Z',
        TOTAL_RECORD: 4,
        TOTAL_ACCEPTED: null,
        TOTAL_REJECT: null,
        TOTAL_FAIL: null,
        STATUS: 'RECEIVED',
        ERROR_MESSAGE: null,
        TRANS_CODE: '411'
    },
    {
        ID: 266287,
        BULK_ID: '456789012',
        SENDING_BIC: 'INDOIDJA',
        REQUEST_DATE: '2023-04-17T09:45:48.081Z',
        TOTAL_RECORD: 4,
        TOTAL_ACCEPTED: null,
        TOTAL_REJECT: null,
        TOTAL_FAIL: null,
        STATUS: 'RECEIVED',
        ERROR_MESSAGE: null,
        TRANS_CODE: '411'
    }
  ]

  const worksheet = workbook.addWorksheet("My Users"); // New Worksheet
  
  // Column for data in excel. key must match data key
  worksheet.columns = [
    { header: "ID.", key: "id", width: 10 }, 
    { header: "BULK ID", key: "b_id", width: 10 },
    { header: "SENDING BIC", key: "bic", width: 10 },
    { header: "STATUS", key: "status", width: 10 },
    { header: "TRANS CODE", key: "trans", width: 10 },
  ];

  await bb.map((value, idx) => {
    worksheet.addRow({
      id : value.ID,
      b_id : value.BULK_ID,
      bic : value.SENDING_BIC,
      status : value.STATUS,
      trans : value.TRANS_CODE
    })
  })
};

module.exports = {buildExcel};
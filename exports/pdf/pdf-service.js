const pdfDoc = require('pdfkit-table');

async function buildPDF(data, dataCallback, endCallback) {
    const doc = new pdfDoc({size: 'A4'});
    // doc.fontSize(10).text(JSON.stringify(data));
    // console.log(data);
    let bb = [
        {
            ID: 266272,
            BULK_ID: '202304170',
            SENDING_BIC: 'INDOIDJA',
            REQUEST_DATE: '2023-04-17T01:11:41.606Z',
            TOTAL_RECORD: 4,
            TOTAL_ACCEPTED: 4,
            TOTAL_REJECT: 0,
            TOTAL_FAIL: 0,
            STATUS: 'COMPLETED',
            ERROR_MESSAGE: null,
            TRANS_CODE: '411'
        },
        {
            ID: 266285,
            BULK_ID: '456789011',
            SENDING_BIC: 'INDOIDJA',
            REQUEST_DATE: '2023-04-17T09:31:43.950Z',
            TOTAL_RECORD: 4,
            TOTAL_ACCEPTED: null,
            TOTAL_REJECT: null,
            TOTAL_FAIL: null,
            STATUS: 'RECEIVED',
            ERROR_MESSAGE: null,
            TRANS_CODE: '411'
        },
        {
            ID: 266287,
            BULK_ID: '456789012',
            SENDING_BIC: 'INDOIDJA',
            REQUEST_DATE: '2023-04-17T09:45:48.081Z',
            TOTAL_RECORD: 4,
            TOTAL_ACCEPTED: null,
            TOTAL_REJECT: null,
            TOTAL_FAIL: null,
            STATUS: 'RECEIVED',
            ERROR_MESSAGE: null,
            TRANS_CODE: '411'
        }
    ]
    doc
    .fillColor("#444444")
    .fontSize(20)
    .text("MASTER DATA.", 50, 57)
    .fontSize(10)
    .text("725 Fowler Avenue", 200, 65, { align: "right" })
    .text("Chamblee, GA 30341", 200, 80, { align: "right" })
    .moveDown()

    let cek = bb.map((value) => {
        value.forEach
        if (value === null) {
            return value = 0;
        }
    });

    console.log(cek);

    const table = {
        headers: [
                    { label: "ID", property: 'ID', width: 60, renderer: null },
                    { label: "BULK ID", property: 'BULK_ID', width: 150, renderer: null }, 
                    { label: "REQUEST_DATE", property: 'REQUEST_DATE', width: 100, renderer: null }, 
                    { label: "TOTAL_RECORD", property: 'TOTAL_RECORD', width: 100, renderer: null }, 
                    { label: "TOTAL_ACCEPTED", property: 'TOTAL_ACCEPTED', width: 80, renderer: null },
                    { label: "TOTAL_REJECT", property: 'TOTAL_REJECT', width: 80, renderer: null },
                    { label: "TOTAL_FAIL", property: 'TOTAL_FAIL', width: 80, renderer: null },
                    { label: "STATUS", property: 'STATUS', width: 80, renderer: null },
                    { label: "ERROR_MESSAGE", property: 'ERROR_MESSAGE', width: 80, renderer: null },
                    { label: "TRANS_CODE", property: 'TRANS_CODE', width: 80, renderer: null },
                ],
        // datas: data
        // rows : bb.map((value) => {
        //     if (value === nul) {
        //         return value = 0;
        //     }
        // })
    }

    // Set up table layout
    const tableLayout = {
        fillColor: function (rowIndex, node, columnIndex) {
        return (rowIndex % 2 === 0) ? '#F0F0F0' : null;
        }
    };

    // Add table header
    // doc.fontSize(12);
    // doc.font('Helvetica-Bold');
    // doc.text('Data BB', { align: 'left' });
    // doc.moveDown();

    // // Add table to document
    // doc.font('Helvetica');
    // doc.fontSize(10);
    await doc.table(table);

    // console.log(table.rows)
    // doc.table(table, { 
    //     columnsSize: [ 200, 100, 100 ],
    // });
    

    
    doc.on('data', dataCallback);
    doc.on('end', endCallback);
    doc.end();
}

module.exports = { buildPDF }

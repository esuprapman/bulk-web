const axios = require('axios');
const pdf = require("pdf-creator-node");
const fs = require("fs");
const path = require('path');
const pup = require('puppeteer');
const mustache = require('mustache');

const getPdfCek = async (req, res) => {
    const response = await axios.get('http://localhost:2000/bulk/master');
    const databulk = response.data;

    const browser = await pup.launch();
    const page = await browser.newPage();

    let html = fs.readFileSync(path.join(__dirname, 'dataMaster.html'), "utf8");

    let bb = [
        {
            ID: 266272,
            BULK_ID: '202304170',
            SENDING_BIC: 'INDOIDJA',
            REQUEST_DATE: '2023-04-17T01:11:41.606Z',
            TOTAL_RECORD: 4,
            TOTAL_ACCEPTED: 4,
            TOTAL_REJECT: 0,
            TOTAL_FAIL: 0,
            STATUS: 'COMPLETED',
            ERROR_MESSAGE: null,
            TRANS_CODE: '411'
        },
        {
            ID: 266285,
            BULK_ID: '456789011',
            SENDING_BIC: 'INDOIDJA',
            REQUEST_DATE: '2023-04-17T09:31:43.950Z',
            TOTAL_RECORD: 4,
            TOTAL_ACCEPTED: null,
            TOTAL_REJECT: null,
            TOTAL_FAIL: null,
            STATUS: 'RECEIVED',
            ERROR_MESSAGE: null,
            TRANS_CODE: '411'
        },
        {
            ID: 266287,
            BULK_ID: '456789012',
            SENDING_BIC: 'INDOIDJA',
            REQUEST_DATE: '2023-04-17T09:45:48.081Z',
            TOTAL_RECORD: 4,
            TOTAL_ACCEPTED: null,
            TOTAL_REJECT: null,
            TOTAL_FAIL: null,
            STATUS: 'RECEIVED',
            ERROR_MESSAGE: null,
            TRANS_CODE: '411'
        }
    ]
    // console.log({data : bb})
    const renderHtml = mustache.render(html, {data : databulk});
    await page.setContent(renderHtml);
    
    try {
        const pdf = await page.pdf({ format: 'A4' });
        
        await browser.close();
        
        console.log(`Generating PDF`);
        res.setHeader('Content-Type', 'application/pdf');
        res.setHeader('Content-Disposition', 'attachment; filename=BulkMaster.pdf');
        res.send(pdf);   
    } catch (error) {
        console.log('export Error : ', error);
    }
}

module.exports = {getPdfCek};
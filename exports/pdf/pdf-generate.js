const pdfService = require('./pdf-service');

const axios = require('axios');



const getPdf = async (req, res) => {
    const response = await axios.get('http://localhost:2000/bulk/master');
    const databulk = response.data;
    // console.log(databulk);
    // res.status(200).json(databulk);

    const stream = res.writeHead(200, {
        'Content-Type': 'application/pdf',
        'Content-Disposition': 'attachment;filename=Bulk.pdf',
    })

    pdfService.buildPDF(databulk, (chunk) => stream.write(chunk), () => stream.end())

}

module.exports = { getPdf };
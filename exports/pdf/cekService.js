const pdf = require("pdf-creator-node");
const fs = require("fs");
const path = require('path');

async function printPDF(data, stream){
    let html = fs.readFileSync(path.join(__dirname, 'dataMaster.html'), "utf8");

    let options = {
    format: "A3",
    orientation: "portrait",
    border: "10mm",
    header: {
        height: "45mm",
        contents: '<div style="text-align: center;">Author: Eggy Suprapman</div>'
    },
    footer: {
        height: "28mm",
        contents: {
            first: 'Cover page',
            2: 'Second page', // Any page number is working. 1-based index
            default: '<span style="color: #444;">{{page}}</span>/<span>{{pages}}</span>', // fallback value
            last: 'Last Page'
        }
    }
    };

    let document = {
        html: html,
        data: {
          master: data,
        },
        path: stream,
        type: "",
    }

    pdf
    .create(document, options)
    .then((res) => {
        console.log(res);
        // res.end();
    })
    .catch((error) => {
        console.error(error);
  });
}

module.exports = { printPDF }
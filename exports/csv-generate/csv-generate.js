const json2csv = require('json2csv').parse
const axios = require('axios');


const getcsv = async (req, res) => {
    const response = await axios.get('http://localhost:2000/bulk/master'); // fetching data
    const databulk = response.data; //response data

    const fields = ['ID', 'BULK_ID', 'SENDING_BIC', 'REQUEST_DATE',
                    'TOTAL_RECORD', 'TOTAL_ACCEPTED', 'TOTAL_REJECT',
                    'TOTAL_FAIL', 'STATUS', 'ERROR_MESSAGE', 'TRANS_CODE'];
              
    const csv = json2csv(databulk, {fields})

    res.writeHead(200, {
        'Content-Type': 'text/csv',
        'Content-Disposition': 'attachment;filename=master.csv',
    });

    res.end(csv);
}

const getCSVDetail = async (req, res) => {
    const params = req.params.id;
    const response = await axios.get(`http://localhost:2000/bulk/detail/${params}`); // fetching data
    const databulk = response.data; //response data

    const fields =  ['BULK_ID', 'RECORD_NO', 'REQUEST_ID',
                    'RESPONSE_DATE', 'SENDING_BIC', 'STATUS',
                    'MESSAGE_ERROR'];
              
    const csv = json2csv(databulk, {fields})

    res.writeHead(200, {
        'Content-Type': 'text/csv',
        'Content-Disposition': 'attachment;filename=master.csv',
    });

    res.end(csv);
}

module.exports = {getcsv, getCSVDetail}
const { getDataDetail, searchBulkDetail } = require('../controllers/bulkDetail');
const getDataHistory = require('../controllers/bulkHistory');
const { getDataMaster, searchMaster } = require('../controllers/bulkMaster');
const { getPdf } = require('../exports/pdf/pdf-generate');
const {getPdfCek } = require('../exports/pdf/cekpdf');
const { getexcel } = require('../exports/excel/excel-generate');
const { getcsv, getCSVDetail } = require('../exports/csv-generate/csv-generate');
const {addData, getData, deleteData, updateData} = require('../controllers/generalSetting')

const express = require('express').Router;

const router = express();

router.get('/bulk/response-history/:id', getDataHistory);
router.get('/bulk/master', getDataMaster);
router.get('/bulk/detail/:id', getDataDetail);

// ============ GENERAL SETTING
router.post('/bulk/general-setting/add', addData);
router.put('/bulk/general-setting/update/:id', updateData);
router.delete('/bulk/general-setting/delete/:id', deleteData);
// router.get('/bulk/general-setting/:id', 'blom ada isi');
router.get('/bulk/general-setting', getData);

// =========== GENERATE EXPORT DETAIL
router.get('/bulk/download-csv/:id', getCSVDetail);

// =========== GENERATE EXPORT MASTER
router.get('/bulk/download-pdf', getPdf);
router.get('/bulk/download-pdfcek', getPdfCek);
router.get('/bulk/download-excel', getexcel);
router.get('/bulk/download-csv', getcsv);

router.post('/bulk/master', searchMaster);
router.post('/bulk/detail/:id', searchBulkDetail);

module.exports = router;